var path = require("path")

const CopyWebpackPlugin = require("copy-webpack-plugin");
const CSSExtractPlugin = require("mini-css-extract-plugin");

const DestPath = path.resolve(__dirname, "dist");
const JSDestPath = path.resolve(DestPath, "src");
const CSSDestPath = path.resolve(DestPath, "style");
const AssetsDestPath = path.resolve(DestPath, "assets");

module.exports = {
	mode: "development",
	devtool: "inline-source-map",
	entry: "./src/index.ts",
	resolve: {extensions: [".ts", ".tsx", ".js"]},
	module: {rules: [
		{test: /\.tsx?$/, loader: "ts-loader"},
		{test: /\.scss$/, use: [
			{loader: CSSExtractPlugin.loader, options: {publicPath: CSSDestPath}},
			{loader: "css-loader", options: {url: false}},
			{loader: "sass-loader"},
		]},
	]},
	output: {filename: "index.js", path: JSDestPath},
	plugins: [
		new CopyWebpackPlugin([
			{from: "index.html", to: DestPath},
			{from: "manifest.json", to: DestPath},
			{from: "assets/", to: AssetsDestPath},
			{from: "worker.js", to: DestPath}, // Copying the service worker file
			// {from: path.join(JSDestPath, "index.css"), to: CSSDestPath}, // TODO: figure out how to get the css styles put in destination directory the right way instead of copying it.
		]),
		new CSSExtractPlugin({filename: "index.css"}),
	]
};
