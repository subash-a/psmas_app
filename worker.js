const str = "hello world";

var images = [
	"assets/images/exreg_bg.png",
	"assets/images/help_bg.png",
	"assets/images/indplan_bg.png",
	"assets/images/login_bg.png",
	"assets/images/logo_psmas.png",
	"assets/images/logo_psmas_192.png",
	"assets/images/map_fg.png",
	"assets/images/newreg_bg.png",
	"assets/images/priplan_bg.png",
	"assets/images/pubplan_bg.png",
	"assets/images/regsucc_bg.png"
];

var fonts = [
	"assets/fonts/Montserrat/Thin.ttf",
	"assets/fonts/Montserrat/Regular.ttf",
	"assets/fonts/Montserrat/Medium.ttf",
	"assets/fonts/Montserrat/Bold.ttf"
];

var js = [
	"src/index.js",
	"worker.js"
];

var others = [
	"index.html",
	"src/index.css",
	"manifest.json"
];
var all = [];
all = all.concat(others, js, fonts, images);

self.addEventListener("install", function(e) {
	e.waitUntil(caches.open("psmas_app").then(function (cache) {
		return cache.addAll(all);
	}));
});

self.addEventListener("fetch", function(e) {
	let url = e.request.url;
	e.respondWith(caches.match(e.request).then(function(response) {
		return response || fetch(e.request);
	}));
});
