import * as React from "react";
import {ViewManager} from "./view_manager";

export class AppShell extends React.Component {
	constructor(props: {}) {
		super(props);
	}

	render(): JSX.Element {
		return (
			<div className="app-shell">
				<ViewManager/>
			</div>
		);
	}
}
