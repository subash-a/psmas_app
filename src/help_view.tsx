import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

interface IHelpViewProps extends ViewProps {}

interface IHelpViewState {
	val: number;
}

export class HelpView extends React.Component<IHelpViewProps, IHelpViewState> {
	constructor(props: IHelpViewProps) {
		super(props);

		this.gotoSignup = this.gotoSignup.bind(this);
		this.handleChange = this.handleChange.bind(this);

		this.state = {val: 0};
	}

	render(): JSX.Element {
		return (
			<div className="help-view">
				<div className="nav-bar">
					<Button onClick={this.props.back}><ArrowBack/></Button>
				</div>
				<div className="title">
					<div>HELP</div>
				</div>
				<div className="help-details">
					<AppBar position="static">
						{/*TODO: Fix the border that appears on top of the tab section*/}
						<Tabs fullWidth value={this.state.val} onChange={this.handleChange}>
							<Tab label="FAQ" />
							<Tab label="Contact Us"/>
						</Tabs>
					</AppBar>
					<div className="tab-view">
						<div className="tab-content">
							<ExpansionPanel>
							<ExpansionPanelSummary>How do I become a PSMAS member?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel expanded={true}>
							<ExpansionPanelSummary>What if I leave my current employer, can I continue with my membership?</ExpansionPanelSummary>
							<ExpansionPanelDetails>Yes, you may visit any of our offices and arrange for payment of subscription via direct debit from your bank, or cash deposits at these branches. You would still maintain the same membership number.</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel>
							<ExpansionPanelSummary>Is my new born baby covered?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel>
							<ExpansionPanelSummary>My child just turned 18, but is still in school, what happens now?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel>
							<ExpansionPanelSummary>I have just gotten married, does my spouse also need to wait three months?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel>
							<ExpansionPanelSummary>I am retiring, does my membership automatically continue?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						<ExpansionPanel>
							<ExpansionPanelSummary>what are waiting periods?</ExpansionPanelSummary>
							<ExpansionPanelDetails>-NA-</ExpansionPanelDetails>
						</ExpansionPanel>
						</div>
					</div>
				</div>
			</div>
		);
	}

	handleChange(evt: React.ChangeEvent, val: number): void {
		this.setState({val: val});
	}

	private gotoSignup() {
		this.props.updateView(ViewStates.SIGNUP, ViewStates.INDIVIDUAL_PLAN, this.state);
	}
}
