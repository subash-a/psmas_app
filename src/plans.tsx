import * as React from "react";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";

interface IPlansViewProps extends ViewProps {}
interface IPlansViewState {}

export class PlansView extends React.Component<IPlansViewProps, IPlansViewState> {
	constructor(props: IPlansViewProps) {
		super(props);

		this.gotoIndividualPlan = this.gotoIndividualPlan.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		let commonCardStyle = {
			borderRadius: "5px",
		};

		let indCardStyle = Object.assign({
			backgroundImage: "url(\"../assets/images/indplan_bg.png\")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "cover",
		}, commonCardStyle);

		let priCardStyle = Object.assign({
			backgroundImage: "url(\"../assets/images/priplan_bg.png\")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "cover",
		}, commonCardStyle);

		let pubCardStyle = Object.assign({
			backgroundImage: "url(\"../assets/images/pubplan_bg.png\")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "cover",
		}, commonCardStyle);

		let cardHeaderStyle = {
			padding: "8% 5% 2% 5%"
		};

		let cardCommentStyle = {
			padding: "2% 5% 8% 5%"
		};

		return (
			<div className="plans-view">
				<div className="nav-bar">
					<Button color="primary" onClick={this.props.back}><ArrowBack/></Button>
				</div>
				<div className="title">
					<div>CHOOSE YOUR PLAN</div>
				</div>
				<div className="plan-list">
					<Card raised className={"ind-card"} onClick={this.gotoIndividualPlan}>
						<CardHeader title="INDIVIDUAL" style={cardHeaderStyle}/>
						<CardContent style={cardCommentStyle}>
							<Typography component="p">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
							</Typography>
						</CardContent>
					</Card>
					<Card raised className={"pri-card"} onClick={this.noOp}>
						<CardHeader title="PRIVATE" style={cardHeaderStyle}/>
						<CardContent style={cardCommentStyle}>
							<Typography component="p">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
							</Typography>
						</CardContent>
					</Card>
					<Card raised className={"pub-card"} onClick={this.noOp}>
						<CardHeader title="PUBLIC SECTOR" style={cardHeaderStyle}/>
						<CardContent style={cardCommentStyle}>
							<Typography component="p">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
							</Typography>
						</CardContent>
					</Card>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoIndividualPlan() {
		this.props.updateView(ViewStates.INDIVIDUAL_PLAN, ViewStates.PLANS, this.state);
	}
}
