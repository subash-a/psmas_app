import * as React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputBase from "@material-ui/core/InputBase";
import {ViewStates, ViewProps} from "./view_manager";
import {withStyles} from "@material-ui/core/styles";
import PersonOutlined from "@material-ui/icons/PersonOutlined";
import VpnKeyOutlined from "@material-ui/icons/VpnKeyOutlined";
import ArrowBack from "@material-ui/icons/ArrowBack";
import ArrowForward from "@material-ui/icons/ArrowForward";
import LocationOn from "@material-ui/icons/LocationOn";

interface ILoginViewProps extends ViewProps {}
interface ILoginViewState {}

interface ButtonStyle {
	root: string;
	light: string;
	bold: string;
}

const lightButtonStyle = {
	root: {
		"text-transform": "none",
		"color": "white",
		"font-family": "Montserrat-Thin",
	}
};
const regularButtonStyle = {
	root: {
		"text-transform": "none",
		"color": "white",
		"font-family": "Montserrat-Regular",
	}
};
const mediumButtonStyle = {
	root: {
		"text-transform": "none",
		"color": "white",
		"font-family": "Montserrat-Medium",
	}
};
const boldButtonStyle = {
	root: {
		"text-transform": "none",
		"color": "white",
		"font-family": "Montserrat-Bold",
	}
};

let BoldButton = withStyles(boldButtonStyle)(Button);
let LightButton = withStyles(lightButtonStyle)(Button);
export let MediumButton = withStyles(mediumButtonStyle)(Button);
let RegularButton = withStyles(regularButtonStyle)(Button);

export class LoginView extends React.Component<ILoginViewProps, ILoginViewState> {
	constructor(props: ILoginViewProps) {
		super(props);

		this.gotoHelp = this.gotoHelp.bind(this);
		this.gotoPlans = this.gotoPlans.bind(this);
		this.gotoLocator = this.gotoLocator.bind(this);
		this.gotoRegistration = this.gotoRegistration.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		return (
				<div className="login-view">
					<div className="background">
						<div className="gradient-bg"></div>
					</div>
					<div className="foreground">
						<div className="top-section">
							<div>
								<div className="logo">
									<div><img src="assets/images/logo_psmas.png"/></div>
									<div><b>WELCOME TO PSMAS</b></div>
								</div>
							</div>
							<div>
								<div className="extended-card">
									<div className="title">PLANS FOR YOU</div>
									<div className="content">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
									</div>
									<div className="browse-link">
										<BoldButton onClick={this.gotoPlans}>Browse our plans <ArrowForward/></BoldButton>
									</div>
								</div>
							</div>
							<div>
								<div className="action-bar">
									<div>
										<RegularButton variant="contained" onClick={this.gotoLocator}> <LocationOn/> Branch Locator </RegularButton>
									</div>
									<div>
										<BoldButton onClick={this.gotoHelp}>Help</BoldButton>
									</div>
								</div>
							</div>
						</div>
						<div className="bottom-section">
							<div className="header"><b>Login</b></div>
							<div className="username-field">
								<TextField fullWidth placeholder="Username" InputProps={{startAdornment: (<InputAdornment position="start"><PersonOutlined/></InputAdornment>)}}/>
							</div>
							<div className="password-field">
								<div><TextField fullWidth placeholder="Password" InputProps={{startAdornment: (<InputAdornment position="start"><VpnKeyOutlined/></InputAdornment>)}}/></div>
								<div><RegularButton>Forgot Password</RegularButton></div>
							</div>
							<div className="action-bar">
								<div><BoldButton onClick={this.gotoRegistration}> New Registration </BoldButton></div>
								<div><Button color="primary" variant="contained" onClick={this.noOp}><ArrowForward/></Button></div>
							</div>
						</div>
					</div>
				</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoRegistration() {
		this.props.updateView(ViewStates.REGISTRATION, ViewStates.LOGIN, this.state);
	}

	private gotoLocator() {
		this.props.updateView(ViewStates.LOCATOR, ViewStates.LOGIN, this.state);
	}

	private gotoPlans() {
		this.props.updateView(ViewStates.PLANS, ViewStates.LOGIN, this.state);
	}

	private gotoHelp() {
		this.props.updateView(ViewStates.HELP, ViewStates.LOGIN, this.state);
	}
}
