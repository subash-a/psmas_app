import * as React from "react";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";

interface IRegistrationViewProps extends ViewProps {}

interface IRegistrationViewState {

}

export class RegistrationView extends React.Component<IRegistrationViewProps, IRegistrationViewState> {
	constructor(props: IRegistrationViewProps) {
		super(props);

		this.gotoPlans = this.gotoPlans.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		let commonCardStyle = {
			borderRadius: "5px",
		};

		let newMemCardStyle = Object.assign({
			backgroundImage: "url(\"../assets/images/newreg_bg.png\")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "100%",
		}, commonCardStyle);

		let exsMemCardStyle = Object.assign({
			backgroundImage: "url(\"../assets/images/exreg_bg.png\")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "100%",
		}, commonCardStyle);

		let cardHeaderStyle = {
			padding: "15% 10% 2% 10%"
		};

		let cardCommentStyle = {
			padding: "2% 10% 4%"
		};

		return (
			<div className="registration-view">
				<div className="nav-bar">
					<Button color="primary" onClick={this.props.back}><ArrowBack/></Button>
				</div>
				<div className="title">
					<div>CHOOSE YOUR REGISTRATION</div>
				</div>
				<div className="registration-list">
					<Card raised className={"newmem-card"} onClick={this.gotoPlans}>
						<CardHeader title="NEW MEMBER REGISTRATION" style={cardHeaderStyle}/>
						<CardContent style={cardCommentStyle}>
							<Typography component="p">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
							</Typography>
						</CardContent>
					</Card>
					<Card raised className={"exsmem-card"} onClick={this.noOp}>
						<CardHeader title="EXISTING MEMBER REGISTRATION" style={cardHeaderStyle}/>
						<CardContent style={cardCommentStyle}>
							<Typography component="p">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
							</Typography>
						</CardContent>
					</Card>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoPlans() {
		this.props.updateView(ViewStates.PLANS, ViewStates.REGISTRATION, this.state);
	}
}
