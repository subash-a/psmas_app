import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {ViewProps} from "./view_manager";

interface IBenificiaryViewProps extends ViewProps {}

interface IBenificiaryViewState {}

export class BenificiaryView extends React.Component<IBenificiaryViewProps, IBenificiaryViewState> {
	constructor(props: IBenificiaryViewProps) {
		super(props);

		this.state = {};
	}

	render(): JSX.Element {
		return (
			<div className="benificiary-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button onClick={this.props.back}>Back</Button></div>
							<div>Add Benificiary Details</div>
							<div></div>
						</div>
						<div className="details-section">
							<div className="details-form">
								<div className="fields">
									<div className="gender-selection">
										<RadioGroup aria-label="Gender" name="gender1">
											<FormControlLabel value="female" control={<Radio />} label="Female" />
											<FormControlLabel value="male" control={<Radio />} label="Male" />
										</RadioGroup>
									</div>
									<div>
										<TextField fullWidth placeholder="Relationship"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Title"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Initials"/>
									</div>
									<div>
										<TextField fullWidth placeholder="First Name"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Surname"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Plan Code"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Birthday" type="date" value={undefined} onChange={undefined}/>
									</div>
									<div>
										<TextField fullWidth placeholder="ID Number"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Benificiary Type"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Marital Status"/>
									</div>
									<div>
										<TextField fullWidth placeholder="Marital Date" type="date" value={undefined} onChange={undefined}/>
									</div>
								</div>
							</div>
							<div>
								<Button variant="contained" onClick={this.noOp}>Add Benificiary</Button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}
}
