import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";

interface IPaymentViewProps {

}

interface IPaymentViewState {

}

export class PaymentView extends React.Component<IPaymentViewProps, IPaymentViewState> {
	constructor(props: {}) {
		super(props);
	}

	render(): JSX.Element {
		return (
			<div className="payment-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button>Back</Button></div>
							<div>Payment Method</div>
							<div></div>
						</div>
						<div className="details-section">
							<div className="card-section">
								<div>VISA</div>
								<div>MSTR</div>
								<div>AMEX</div>
								<div>DISC</div>
							</div>
							<div className="amount-section">
								<div>
									<div>Payment Amount</div>
									<div>$3,000</div>
								</div>
								<Button>Edit</Button>
							</div>
							<div className="info-section">
								<div>
									<TextField fullWidth placeholder="Name On Card"></TextField>
								</div>
								<div>
									<TextField fullWidth placeholder="Card Number"></TextField>
								</div>
								<div className="card-details">
								<FormControl>
									<InputLabel htmlFor="month-helper">Month</InputLabel>
									<Select	input={<Input name="month" id="month-helper" />}>
										<MenuItem value={10}>Ten</MenuItem>
										<MenuItem value={20}>Twenty</MenuItem>
										<MenuItem value={30}>Thirty</MenuItem>
									</Select>
								</FormControl>
								<FormControl>
									<InputLabel htmlFor="year-helper">Year</InputLabel>
									<Select	input={<Input name="year" id="year-helper" />}>
										<MenuItem value={10}>Ten</MenuItem>
										<MenuItem value={20}>Twenty</MenuItem>
										<MenuItem value={30}>Thirty</MenuItem>
									</Select>
								</FormControl>
								<div>
									<TextField fullWidth placeholder="CVV" InputProps={{endAdornment: <InputAdornment position="end">Help</InputAdornment>}}></TextField>
								</div>
								</div>
								<div>
									<TextField fullWidth placeholder="Zip/Postal Code" InputProps={{endAdornment: <InputAdornment position="end">Help</InputAdornment>}}></TextField>
								</div>
							</div>
							<Button color="primary" variant="contained">Make Payment</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
