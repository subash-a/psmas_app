import * as React from "react";
import Button from "@material-ui/core/Button";
import ButtonBase from "@material-ui/core/ButtonBase";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Person from "@material-ui/icons/Person";
import Payment from "@material-ui/icons/Payment";
import BusinessCenter from "@material-ui/icons/BusinessCenter";
import Dashboard from "@material-ui/icons/Dashboard";
import Event from "@material-ui/icons/Event";

interface IPaymentsViewProps extends ViewProps {}

interface IPaymentsViewState {

}

export class PaymentsView extends React.Component<IPaymentsViewProps, IPaymentsViewState> {
	constructor(props: IPaymentsViewProps) {
		super(props);

		this.gotoClaims = this.gotoClaims.bind(this);
		this.gotoProfile = this.gotoProfile.bind(this);
		this.gotoDashboard = this.gotoDashboard.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		let tabStyle = {
			fontSize: "1rem",
			background: "transparent",
		};

		let paymentTabContent = (
			<div className="payment-tab-content">
				<div className="breakdown">
					<div>
						<div>Policy Number</div>
						<div>DIC123456789</div>
					</div>
					<div>
						<div>Due Date</div>
						<div>22 Dec 2018</div>
					</div>
					<div>
						<div>Due Amount</div>
						<div>$2,000</div>
					</div>
					<div>
						<div>Arrear Amount</div>
						<div>$1,000</div>
					</div>
					<Divider/>
					<div>
						<div>Total Amount</div>
						<div>$3,000</div>
					</div>
				</div>
				<div className="total">
					<div>
						<div>Amount To Pay</div>
						<div>
							<TextField fullWidth placeholder="3,000" InputProps={{startAdornment: <InputAdornment position="start">$</InputAdornment>}}/>
						</div>
					</div>
					<Button color="primary" variant="contained">Proceed To Payment</Button>
				</div>
				<div></div>
			</div>
		);

		let historyTabContent = (
			<div className="history-tab-content">
				<div className="payment-list">
					<div className="payment-details">
						<div>
							<div>DIC123456789</div>
							<div>Credit Card</div>
							<div><Event/><div>22 Feb 2018</div></div>
						</div>
						<div>
							<div>$2,000</div>
							<div><Chip label="Success" variant="outlined"/></div>
						</div>
					</div>
					<div className="payment-details">
						<div>
							<div>DIC123456789</div>
							<div>Credit Card</div>
							<div><Event/><div>22 Feb 2018</div></div>
						</div>
						<div>
							<div>$2,000</div>
							<div><Chip label="Success" variant="outlined"/></div>
						</div>
					</div>
					<div className="payment-details">
						<div>
							<div>DIC123456789</div>
							<div>Credit Card</div>
							<div><Event/><div>22 Feb 2018</div></div>
						</div>
						<div>
							<div>$2,000</div>
							<div><Chip label="Success" variant="outlined"/></div>
						</div>
					</div>
					<div className="payment-details">
						<div>
							<div>DIC123456789</div>
							<div>Credit Card</div>
							<div><Event/><div>22 Feb 2018</div></div>
						</div>
						<div>
							<div>$2,000</div>
							<div><Chip label="Success" variant="outlined"/></div>
						</div>
					</div>
					<div className="payment-details">
						<div>
							<div>DIC123456789</div>
							<div>Credit Card</div>
							<div><Event/><div>22 Feb 2018</div></div>
						</div>
						<div>
							<div>$2,000</div>
							<div><Chip label="Success" variant="outlined"/></div>
						</div>
					</div>
				</div>
			</div>
		);

		return (
			<div className="payments-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button onClick={this.props.back}><ArrowBack/></Button></div>
							<div>Payments</div>
							<div></div>
						</div>
						<div className="details-section">
							<AppBar position="static">
								<Tabs fullWidth value={0}>
									<Tab label="Current Payment" style={tabStyle}/>
									<Tab label="Payment History" style={tabStyle}/>
								</Tabs>
							</AppBar>
							{historyTabContent}
						</div>
						<div className="footer">
							<ButtonBase onClick={this.gotoDashboard}>
								<div>
									<div><Dashboard/></div>
									<div>Dashboard</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.noOp}>
								<div>
									<div><Payment/></div>
									<div>Payment</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.gotoClaims}>
								<div>
									<div><BusinessCenter/></div>
									<div>Claim</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.gotoProfile}>
								<div>
									<div><Person/></div>
									<div>Profile</div>
								</div>
							</ButtonBase>
						</div>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoDashboard() {
		this.props.updateView(ViewStates.DASHBOARD, ViewStates.PAYMENTS, this.state);
	}

	private gotoClaims() {
		this.props.updateView(ViewStates.CLAIMS, ViewStates.PAYMENTS, this.state);
	}

	private gotoProfile() {
		this.props.updateView(ViewStates.PROFILE, ViewStates.PAYMENTS, this.state);
	}
}
