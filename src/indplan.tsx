import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";

interface IPlanViewProps extends ViewProps {}

interface IPlanViewState {
	val: number;
}

export class PlanView extends React.Component<IPlanViewProps, IPlanViewState> {
	constructor(props: IPlanViewProps) {
		super(props);

		this.gotoSignup = this.gotoSignup.bind(this);
		this.handleChange = this.handleChange.bind(this);

		this.state = {val: 0};
	}

	render(): JSX.Element {
		return (
			<div className="plan-view">
				<div className="nav-bar">
					<Button onClick={this.props.back}><ArrowBack/></Button>
				</div>
				<div className="title">
					<div>SELECT INDIVIDUAL PLAN</div>
				</div>
				<div className="plan-details">
					<AppBar position="static">
						{/*TODO: Fix the border that appears on top of the tab section*/}
						<Tabs fullWidth value={this.state.val} onChange={this.handleChange}>
							<Tab label="Shield" />
							<Tab label="Flexi health"/>
							<Tab label="Individual"/>
						</Tabs>
					</AppBar>
					<div className="tab-view">
						<div>
							<div className="sec-title"><b>Subscriptions</b></div>
							<ul>
								<li>Member $10</li>
								<li>1st Benificiary $10</li>
								<li>2nd and Sub Ben $6</li>
								<li>Adult Ben $10</li>
							</ul>
						</div>
						<div>
							<div className="sec-title"><b>GLOBAL LIMIT</b></div>
							<div>$3000 per annum</div>
						</div>
						<div>
							<div className="sec-title"><b>General Practitioners</b></div>
							<div>
								Paid in full according to tariff at Municipal, Mission and Government Hospitals for the Scheme up to 6 visits per annum per member
							</div>
						</div>
						<div>
							<div className="sec-title"><b>Medical Specialists</b></div>
							<div>
								Paid in full according to tariff at Municipal, Mission and Government Hospitals for the Scheme up to 6 visits per annum per member
							</div>
						</div>
						<div>
							<div className="sec-title"><b>Hospitalisation</b></div>
							<div>
								Paid in full according to tariff at Municipal, Mission and Government Hospitals for the Scheme up to 6 visits per annum per member
							</div>
						</div>
						<div>
							<div className="sec-title"><b>Rehabilitation Hospitalisation</b></div>
							<div>
								Paid in full according to tariff at Municipal, Mission and Government Hospitals for the Scheme up to 6 visits per annum per member
							</div>
						</div>
						<div>
							<Button color="primary" variant="contained" onClick={this.gotoSignup}> Grab this </Button>
						</div>
					</div>
				</div>
			</div>
		);
	}

	handleChange(evt: React.ChangeEvent, val: number): void {
		this.setState({val: val});
	}

	private gotoSignup() {
		this.props.updateView(ViewStates.SIGNUP, ViewStates.INDIVIDUAL_PLAN, this.state);
	}
}
