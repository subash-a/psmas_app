import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowForward from "@material-ui/icons/ArrowForward";
import ArrowBack from "@material-ui/icons/ArrowBack";
import MicNone from "@material-ui/icons/MicNone";
import Edit from "@material-ui/icons/Edit";
import Wc from "@material-ui/icons/Wc";

enum SignupStates {
	TITLE = 0,
	INITIALS,
	NAME,
	GENDER,
	SUCCESS,
}

interface ISignupViewProps extends ViewProps {}

interface ISignupViewState {
	view: SignupStates;
}

export class SignupView extends React.Component<ISignupViewProps, ISignupViewState> {
	constructor(props: ISignupViewProps) {
		super(props);

		this.state = {view: SignupStates.TITLE};
	}

	render(): JSX.Element {
		let v = this.state.view;
		let next: () => void;
		let prev: () => void;
		let detailsEntered = [] as Array<JSX.Element>;
		let question: JSX.Element;
		let chipLabel = `${v}/6`;
		switch(v) {
			case SignupStates.TITLE:
				next = this.updateView.bind(this, SignupStates.INITIALS);
				prev = this.props.back;
				question = (
					<div className="title-selection">
						<div>Your Title Please</div>
					</div>
				);
				break;
			case SignupStates.INITIALS:
				next = this.updateView.bind(this, SignupStates.NAME);
				prev = this.updateView.bind(this, SignupStates.TITLE);
				detailsEntered.push(<div key={0}>My Title is <b>Mr.</b><div><Edit/></div></div>);
				question = (
					<div className="initials-selection">
						<div>Your Initials Please</div>
					</div>
				);
				break;
			case SignupStates.NAME:
				next = this.updateView.bind(this, SignupStates.GENDER);
				prev = this.updateView.bind(this, SignupStates.INITIALS);
				detailsEntered.push(<div key={0}>My Title is <b>Mr.</b><div><Edit/></div></div>);
				detailsEntered.push(<div key={1}>My Name is <b>John</b><div><Edit/></div></div>);
				question = (
					<div className="name-selection">
						<div>Your Full Name Please</div>
					</div>
				);
				break;
			case SignupStates.GENDER:
				next = this.updateView.bind(this, SignupStates.SUCCESS);
				prev = this.updateView.bind(this, SignupStates.NAME);
				detailsEntered.push(<div key={0}>My Title is <b>Mr.</b><div><Edit/></div></div>);
				detailsEntered.push(<div key={1}>My Name is <b>John</b><div><Edit/></div></div>);
				detailsEntered.push(<div key={2}>My Username is <b>sklpl</b><div><Edit/></div></div>);
				question = (
					<div className="gender-selection">
						<div>Select your gender</div>
						<div>
							<Button onClick={this.noOp}>
								<div><Wc/></div>
								<div>Male</div>
							</Button>
							<Button onClick={this.noOp}>
								<div><Wc/></div>
								<div>Female</div>
							</Button>
						</div>
					</div>
				);
				break;
			case SignupStates.SUCCESS:
				next = this.gotoDashboard.bind(this);
				prev = this.updateView.bind(this, SignupStates.GENDER);
				break;
		}

		let content = (
			<div className="container">
				<div className="header-section">
					<div><Button onClick={prev}><ArrowBack/></Button></div>
					<div>
						<div>INDIVIDUAL MEMBER SIGNUP FORM</div>
						<div>You will be asked about your <b>personal information</b> and <b>date of birth</b></div>
					</div>
					<div></div>
				</div>
				<div className="details-section">
					<div className="progress">
						<Chip label={chipLabel} variant="default"/>
						<div>Questions answered related to personal information</div>
					</div>
					<div className="details-entered">{detailsEntered}</div>
					<div className="question">{question}</div>
				</div>
				<div className="footer">
					<div><MicNone/></div>
					<div>Skip</div>
					<div>
						<TextField fullWidth placeholder="Title"/>
					</div>
					<div>
						<Button variant="contained" onClick={next}><ArrowForward/></Button>
					</div>
				</div>
			</div>
		);

		if(v === SignupStates.SUCCESS) {
			content = (
				<div className="container" onClick={next}>
					<div className="header-section">
						<div><Button onClick={prev}><ArrowBack/></Button></div>
						<div>
							<div>TRANSACTION IS SUCCESSFUL</div>
							<div></div>
						</div>
						<div></div>
					</div>
					<div className="success-message">
						<div></div>
					</div>
					<div className="empty-footer"></div>
				</div>
			);
		}

		return (
			<div className="signup-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">{content}</div>
			</div>
		);
	}

	private gotoDashboard() {
		this.props.updateView(ViewStates.DASHBOARD, ViewStates.SIGNUP, this.state);
	}

	private updateView(v: SignupStates): void {
		this.setState({view: v});
	}

	private noOp() {
		return false;
	}
}
