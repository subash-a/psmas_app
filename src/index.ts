import * as React from "react";
import * as ReactDOM from "react-dom";
import {AppShell} from "./shell";

import "../style/index.scss";

function startApp(): void {
	let container = document.getElementById("app-container");
	window.onresize = location.reload;
	ReactDOM.render(React.createElement(AppShell, {}), container);
}

startApp();
