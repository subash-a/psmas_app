import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Avatar from "@material-ui/core/Avatar";
import ButtonBase from "@material-ui/core/ButtonBase";
import Chip from "@material-ui/core/Chip";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Person from "@material-ui/icons/Person";
import Payment from "@material-ui/icons/Payment";
import BusinessCenter from "@material-ui/icons/BusinessCenter";
import Dashboard from "@material-ui/icons/Dashboard";

interface IDashboardViewProps extends ViewProps {}

interface IDashboardViewState {}

export class DashboardView extends React.Component<IDashboardViewProps, IDashboardViewState> {
	constructor(props: IDashboardViewProps) {
		super(props);

		this.gotoClaims = this.gotoClaims.bind(this);
		this.gotoProfile = this.gotoProfile.bind(this);
		this.gotoPayments = this.gotoPayments.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		return (
			<div className="home-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="mid"></div>
					<div className="button"></div>
				</div>
				<div className="view-foreground">
					<div className="header-section">
						<div>
							<div>
								<Button color="primary" onClick={this.props.back}><ArrowBack/></Button>
							</div>
							<div>
								Dashboard
							</div>
						</div>
					</div>
					<div className="content-section">
						<div className="profile-section">
							<div className="photo-bar">
								<div></div>
								<div></div>
							</div>
							<div className="detail-bar">
								<div className="name-label">
									<div>
										<div>Michael Jordan</div>
										<div>CID123456789</div>
									</div>
									<div>
										<Chip label="Plan Name"/>
									</div>
								</div>
								<div className="detail-label">
									<div className="detail-part">
										<div>Policy Number</div>
										<div>23456789</div>
									</div>
									<div className="detail-part">
										<div>Due Amount</div>
										<div>$3000</div>
									</div>
									<div className="detail-part">
										<div>Full Amount</div>
										<div>234578</div>
									</div>
									<div className="detail-part">
										<div>Due Date</div>
										<div>12 Dec 2018</div>
									</div>
								</div>
							</div>
						</div>
						<div className="options">
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>Online Payment</div>
								</div>
							</ButtonBase>
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>My Plans</div>
								</div>
							</ButtonBase>
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>Claim Status</div>
								</div>
							</ButtonBase>
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>PSMAS Locator</div>
								</div>
							</ButtonBase>
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>Feedback</div>
								</div>
							</ButtonBase>
							<ButtonBase>
								<div className="sel-option">
									<div></div>
									<div>History</div>
								</div>
							</ButtonBase>
						</div>
					</div>
					<div className="footer">
						<ButtonBase onClick={this.noOp}>
							<div>
								<div><Dashboard/></div>
								<div>Dashboard</div>
							</div>
						</ButtonBase>
						<ButtonBase onClick={this.gotoPayments}>
							<div>
								<div><Payment/></div>
								<div>Payment</div>
							</div>
						</ButtonBase>
						<ButtonBase onClick={this.gotoClaims}>
							<div>
								<div><BusinessCenter/></div>
								<div>Claim</div>
							</div>
						</ButtonBase>
						<ButtonBase onClick={this.gotoProfile}>
							<div>
								<div><Person/></div>
								<div>Profile</div>
							</div>
						</ButtonBase>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoPayments() {
		this.props.updateView(ViewStates.PAYMENTS, ViewStates.DASHBOARD, this.state);
	}

	private gotoClaims() {
		this.props.updateView(ViewStates.CLAIMS, ViewStates.DASHBOARD, this.state);
	}

	private gotoProfile() {
		this.props.updateView(ViewStates.PROFILE, ViewStates.DASHBOARD, this.state);
	}
}
