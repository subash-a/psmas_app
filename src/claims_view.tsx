import * as React from "react";
import Button from "@material-ui/core/Button";
import ButtonBase from "@material-ui/core/ButtonBase";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Person from "@material-ui/icons/Person";
import Payment from "@material-ui/icons/Payment";
import BusinessCenter from "@material-ui/icons/BusinessCenter";
import Dashboard from "@material-ui/icons/Dashboard";
import InsertDriveFile from "@material-ui/icons/InsertDriveFile";

interface IClaimsViewProps extends ViewProps {}

interface IClaimsViewState {

}

export class ClaimsView extends React.Component<IClaimsViewProps, IClaimsViewState> {
	constructor(props: IClaimsViewProps) {
		super(props);

		this.gotoDashboard = this.gotoDashboard.bind(this);
		this.gotoProfile = this.gotoProfile.bind(this);
		this.gotoPayments = this.gotoPayments.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		return (
			<div className="claims-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button onClick={this.props.back}><ArrowBack/></Button></div>
							<div>My Claims</div>
							<div></div>
						</div>
						<div className="details-section">
							<div>
								<AppBar position="static">
									<Tabs fullWidth value={1}>
										<Tab label="Open Claims"/>
										<Tab label="Closed Claims"/>
									</Tabs>
								</AppBar>
								<div className="closed-claims-tab">
									<div className="claims-list">
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
										<div className="closed-claim">
											<div className="claim-detail">
												<div>
													<div>Michael Jordan</div>
													<div>DIC123456789</div>
													<div>22 Feb 2017</div>
												</div>
												<div>
													<div>$2,000</div>
													<div>
														<Chip label="Closed" variant="outlined"/>
													</div>
												</div>
											</div>
											<div className="file-link">
												<div>
													<div><InsertDriveFile/></div>
													<div>Settlement Details</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="footer">
							<ButtonBase onClick={this.gotoDashboard}>
								<div>
									<div><Dashboard/></div>
									<div>Dashboard</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.gotoPayments}>
								<div>
									<div><Payment/></div>
									<div>Payment</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.noOp}>
								<div>
									<div><BusinessCenter/></div>
									<div>Claim</div>
								</div>
							</ButtonBase>
							<ButtonBase onClick={this.gotoProfile}>
								<div>
									<div><Person/></div>
									<div>Profile</div>
								</div>
							</ButtonBase>
						</div>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoPayments() {
		this.props.updateView(ViewStates.PAYMENTS, ViewStates.CLAIMS, this.state);
	}

	private gotoDashboard() {
		this.props.updateView(ViewStates.DASHBOARD, ViewStates.CLAIMS, this.state);
	}

	private gotoProfile() {
		this.props.updateView(ViewStates.PROFILE, ViewStates.CLAIMS, this.state);
	}
}
