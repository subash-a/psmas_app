import * as React from "react";
import {LoginView} from "./login";
import {RegistrationView} from "./registration";
import {PlansView} from "./plans";
import {PlanView} from "./indplan";
import {DashboardView} from "./dashboard_view";
import {ProfileView} from "./profile_view";
import {PaymentView} from "./payment_view";
import {PaymentsView} from "./payments";
import {SignupView} from "./signup_view";
import {BenificiaryView} from "./benificiary_view";
import {ClaimsView} from "./claims_view";
import {LocationView} from "./location_view";
import {LocatorView} from "./locator_view";
import {HelpView} from "./help_view";

export interface ViewStateObject {
	state: Object;
	view: ViewStates;
}

export interface ViewProps {
	initState: Object;
	updateView: (n: ViewStates, o: ViewStates, s: Object) => void;
	back: () => void;
}

export enum ViewStates {
	LOGIN = 0,
	REGISTRATION,
	PLANS,
	INDIVIDUAL_PLAN,
	SIGNUP,
	LOCATOR,
	BENIFICIARY,
	DASHBOARD,
	PROFILE,
	PAYMENTS,
	CLAIMS,
	FEEDBACK,
	HELP,
}

interface IViewManagerProps {

}

interface IViewManagerState {
	view?: ViewStates;
	initstate?: Object;
	viewstack?: Array<ViewStateObject>
}

export class ViewManager extends React.Component<IViewManagerProps, IViewManagerState> {
	constructor(p: IViewManagerProps) {
		super(p);

		this.updateView = this.updateView.bind(this);
		this.back = this.back.bind(this);

		this.state = {
			view: ViewStates.LOGIN,
			initstate: {},
			viewstack: [] as Array<ViewStateObject>
		};
	}

	render(): JSX.Element {

		let view: JSX.Element;
		let initState = this.state.initstate;
		let defaultProps = {initState: initState, updateView: this.updateView, back: this.back};

		switch(this.state.view) {
			case ViewStates.REGISTRATION:
				view = <RegistrationView {...defaultProps}/>;
				break;
			case ViewStates.PLANS:
				view = <PlansView {...defaultProps}/>;
				break;
			case ViewStates.INDIVIDUAL_PLAN:
				view = <PlanView {...defaultProps}/>;
				break;
			case ViewStates.SIGNUP:
				view = <SignupView {...defaultProps}/>;
				break;
			case ViewStates.LOCATOR:
				view = <LocatorView {...defaultProps}/>;
				break;
			case ViewStates.BENIFICIARY:
				view = <BenificiaryView  {...defaultProps}/>;
				break;
			case ViewStates.DASHBOARD:
				view = <DashboardView  {...defaultProps}/>;
				break;
			case ViewStates.PROFILE:
				view = <ProfileView {...defaultProps}/>;
				break;
			case ViewStates.PAYMENTS:
				view = <PaymentsView {...defaultProps}/>;
				break;
			case ViewStates.CLAIMS:
				view = <ClaimsView {...defaultProps}/>;
				break;
			case ViewStates.HELP:
				view = <HelpView {...defaultProps}/>;
				break;
			default:
				view = <LoginView  {...defaultProps}/>;
				break;
		}

		return view;
	}

	private updateView(niw: ViewStates, old: ViewStates, s: Object) {
		let vs = this.state.viewstack;
		vs.push({state: s, view: old});
		this.setState({
			view: niw,
			viewstack: vs,
		});
	}

	private back() {
		let vs = this.state.viewstack;
		let vso = vs.pop();
		this.setState({
			view: vso.view,
			viewstack: vs,
		});
	}
}
