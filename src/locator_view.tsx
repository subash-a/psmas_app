import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import {MediumButton} from "./login";

interface ILocatorViewProps extends ViewProps {}

interface ILocatorViewState {}

export class LocatorView extends React.Component<ILocatorViewProps, ILocatorViewState> {
	constructor(props: ILocatorViewProps) {
		super(props);
	}

	render(): JSX.Element {
		let tabStyle = {
			fontSize: "1.2rem",
			background: "transparent",
		};
		return (
			<div className="locator-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button onClick={this.props.back}><ArrowBack/></Button></div>
							<div>Branch Location</div>
							<div></div>
						</div>
						<div className="details-section">
							<div className="locator-tab">
								<FormControl>
									<InputLabel htmlFor="br-location">Select Location</InputLabel>
									<Select value={""} onChange={this.noOp} inputProps={{name:"br-location"}}>
										<MenuItem value={"bangalore"}>Bangalore</MenuItem>
									</Select>
								</FormControl>
							</div>
							<div className="locator-tab-content">
								<div className="map-view">
									<div></div>
								</div>
								<div>
									<MediumButton variant="contained"> Get Details </MediumButton>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}
}
