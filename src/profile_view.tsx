import * as React from "react";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import {ViewStates, ViewProps} from "./view_manager";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Person from "@material-ui/icons/Person";
import Edit from "@material-ui/icons/Edit";
import Lock from "@material-ui/icons/Lock";
import Forum from "@material-ui/icons/Forum";
import Input from "@material-ui/icons/Input";

interface IProfileViewProps extends ViewProps {}

interface IProfileViewState {

}

export class ProfileView extends React.Component<IProfileViewProps, IProfileViewState> {
	constructor(props: IProfileViewProps) {
		super(props);

		this.gotoBenificary = this.gotoBenificary.bind(this);

		this.state = {};
	}

	render(): JSX.Element {
		let listTextStyle = {fontSize: "1.2rem"};
		return (
			<div className="profile-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="mid"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div>
								<Button color="primary" onClick={this.props.back}><ArrowBack/></Button>
							</div>
							<div>Profile</div>
							<div>
								<Button color="primary" onClick={this.noOp}>Edit</Button>
							</div>
						</div>
						<div className="content-section">
							<div><Avatar src=""/></div>
							<div>
								<div className="full-name">Michael Jordan</div>
								<div className="cid">CID123456789</div>
							</div>
						</div>
						<div></div>
						<div className="profile-section">
							<List component="ul">
								<ListItem button onClick={this.gotoBenificary}>
									<ListItemIcon><div><Person/></div></ListItemIcon>
									<ListItemText primary="Beneficiary Details" />
								</ListItem>
								<Divider/>
								<ListItem button>
									<ListItemIcon><div><Edit/></div></ListItemIcon>
									<ListItemText primary="Edit Profile" />
								</ListItem>
								<Divider/>
								<ListItem button>
									<ListItemIcon><div><Lock/></div></ListItemIcon>
								<ListItemText primary="Change Password" />
								</ListItem>
								<Divider/>
								<ListItem button>
									<ListItemIcon><div><Forum/></div></ListItemIcon>
									<ListItemText primary="Feedback" />
								</ListItem>
								<Divider/>
								<ListItem button>
								<ListItemIcon><div><Input/></div></ListItemIcon>
								<ListItemText primary="Log Out" />
								</ListItem>
							</List>
						</div>
					</div>
				</div>
			</div>
		);
	}

	private noOp() {
		return false;
	}

	private gotoBenificary() {
		this.props.updateView(ViewStates.BENIFICIARY, ViewStates.PROFILE, this.state);
	}
}
