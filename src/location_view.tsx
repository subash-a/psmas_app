import * as React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Chip from "@material-ui/core/Chip";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";

interface ILocationViewProps {

}

interface ILocationViewState {

}

export class LocationView extends React.Component<ILocationViewProps, ILocationViewState> {
	constructor(props: {}) {
		super(props);
	}

	render(): JSX.Element {
		let tabStyle = {
			fontSize: "1.2rem",
			background: "transparent",
		};
		return (
			<div className="location-view">
				<div className="view-background">
					<div className="top"></div>
					<div className="bottom"></div>
				</div>
				<div className="view-foreground">
					<div className="container">
						<div className="header-section">
							<div><Button>Back</Button></div>
							<div>Provider Location</div>
							<div></div>
						</div>
						<div className="details-section">
							<div className="location-tab">
								<AppBar position="static">
									<Tabs fullWidth value={1}>
										<Tab label="list" style={tabStyle}/>
										<Tab label="location" style={tabStyle}/>
										<Tab label="favourite" style={tabStyle}/>
									</Tabs>
								</AppBar>
								<div className="location-tab-content">
									<div className="map-view">Map</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
