#!/bin/bash

# determine ip address
ip_addr=$(awk '/inet / {print $2}')
port=9090

pushd dist > /dev/null {
	# start http-server on localhost
	../node_modules/.bin/http-server -a $ip_addr -p $port
}
