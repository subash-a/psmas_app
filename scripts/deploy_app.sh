#!/bin/bash

hostname="ec2-3-17-12-253.us-east-2.compute.amazonaws.com"
pubip=3.17.12.253
id=techurate.pem
user="ubuntu"

# copy over the dist directory
scp -r -i $id ../dist $user@$hostname:~/psmas_app
